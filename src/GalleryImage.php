<?php

namespace studiocreativateam\Models;

class GalleryImage extends SortedImage
{
    private static $table_name = 'GalleryImage';

    private static $has_one = [
        'Gallery' => 'studiocreativateam\Elemental\Models\ElementGallery',
    ];
}