<?php

namespace studiocreativateam\Elemental\Models;

use Axllent\TiledGridField\TiledGridField;
use Colymba\BulkUpload\BulkUploader;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementGallery extends BaseElement
{
    private static $table_name = 'ElementGallery';

    private static $db = [];

    private static $has_many = [
        'Items' => 'studiocreativateam\Models\GalleryImage',
    ];

    private static $owns = ['Items'];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Root.Items');

        if ($this->ID) {
            $config = GridFieldConfig_RecordEditor::create(50);
            $config->addComponent($bu = new BulkUploader());
            $bu->setUfSetup('setFolderName', 'gallery');
            $config->addComponent(new GridFieldOrderableRows('SortOrder'));
            $fields->removeByName('Items');
            $fields->addFieldToTab('Root.Main', TiledGridField::create('Items', $this->Type, $this->Items(), $config));
        }

        return $fields;
    }

    public function getType()
    {
        $classParts = explode('\\', $this->ClassName);
        return _t(__CLASS__ . '.BlockType', preg_replace('/^Element/', '', array_pop($classParts)));
    }

    public function publishRecursive()
    {
        foreach ($this->Items() as $item) {
            if ($image = $item->Image()) $image->publishSingle();
        }

        return parent::publishRecursive();
    }
}